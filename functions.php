<?php

/**
 * 
 * Theme setup
 * @author Everaldo Matias <https://everaldo.dev>
 * 
 */
function excellence_theme_setup() {
    add_theme_support( 'custom-logo', [
        'height' => 150,
        'width'  => 350
    ] );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
}
add_action( 'after_setup_theme', 'excellence_theme_setup' );

/**
 * 
 */
function excellence_enqueue_scripts() {
    wp_enqueue_style( 'excellence-style', get_template_directory_uri() . '/assets/css/style.css' );
    wp_enqueue_script( 'excellence-scripts', get_template_directory_uri() . '/assets/js/main.js' );
}
add_action( 'wp_enqueue_scripts', 'excellence_enqueue_scripts' );


/**
 * 
 * Function to print custom logo or the site title
 * @author  Everaldo Matias <https://everaldo.dev>
 * @param   $echo_site_title boolean to print site title if logo not set
 * @todo    [23/12/2019] Improvement file to template functions
 * 
 */
function excellence_custom_logo( $echo_site_title = true ) {

    $echo_site_title = (boolean) $echo_site_title;

    if ( has_custom_logo() ) {
        echo '<figure class="wrap-custom-logo">';
            the_custom_logo();
        echo '</figure>';
    } elseif( $echo_site_title ) {
        echo '<h1 class="site-title"><a href="' . esc_url( home_url( '/' ) ) . '">' . esc_html( get_bloginfo( 'name' ) ) . '</a></h1>';
    };

}

/**
 * 
 * Create hook get_template_content
 * @author  Everaldo Matias <https://everaldo.dev>
 * @param   $name of the template to add
 * @todo    [18/11/2019] Improvement load templates to all conditionals
 * 
 */
function get_template_content( $name = null ) {
    
    do_action( 'get_template_content', $name );
 
    $templates = array();
    $name      = (string) $name;
    if ( '' !== $name ) {
        $templates[] = "{$name}.php";
    } elseif( is_front_page() ) {
        $templates[] = "templates/front-page.php";
    } else {
        $type = get_post_type();
        $templates[] = "templates/{$type}.php";
    }
  
    locate_template( $templates, true );
    
}

/**
 * The Extra Functions
 */
require( 'inc/extra-functions.php' );

/**
 * The Featured Page on Home
 */
require( 'inc/modules/featured-page-home/excellence-featured-page-home.php' );

/**
 * Módulo Galeria de Fotos e Vídeos
 */
require( 'inc/modules/media-gallery/excellence-media-gallery.php' );

/**
 * The Social Icons
 */
require( 'inc/classes/excellence-social-icons.php' );

/**
 * Popup
 */
require( 'inc/modules/popup/excellence-popup.php' );

