<?php ob_start(); ?>
<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php wp_body_open(); ?>

		<header id="site-header" class="header-footer-group" role="banner">

			<?php excellence_custom_logo(); ?>
			<?php do_action( 'excellence_header' ); ?>

		</header><!-- #site-header -->
