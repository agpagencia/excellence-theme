<?php

if ( ! class_exists( 'Excellence_Customizer_Social_Icons' ) ) {

    class Excellence_Customizer_Social_Icons {

        private static $instance;

        public static function get_instance() {

            if ( null == self::$instance ) {
                self::$instance = new self;
            }
            return self::$instance;

        }

        private function __construct() {

            /**
             * Verify if Kirki is activated.
             */
            if ( class_exists( 'Kirki' ) ) :
                $this->add_config();
                $this->add_sections();
                $this->add_fields();
            else:
                add_action( 'admin_notices', [$this, 'add_notices'] );
            endif;            

        }

        protected function add_config() {
            Kirki::add_config( 'kirki_custom_config', array(
                'capability'    => 'edit_theme_options',
                'option_type'   => 'theme_mod',
            ) );
        }

        protected function add_sections() {

            /**
             * Social
             * ==============================================================================
             */
            Kirki::add_section( 'social', array(
                'title'          => __( 'Redes Sociais' ),
                'panel'          => '', // Not typically needed.
                'priority'       => 10,
                'capability'     => 'edit_theme_options',
                'theme_supports' => '', // Rarely needed.
            ) );
  
        }

        protected function add_fields() {

            /**
             * Social
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'instagram',
                'label'       => __( 'Instagram', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Instagram', 'odin' ),
                'priority'		=> 10,
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'facebook',
                'label'       => __( 'Facebook', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Facebook', 'odin' ),
                'priority'		=> 10,
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'linkedin',
                'label'       => __( 'Linkedin', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Linkedin', 'odin' ),
                'priority'		=> 10,
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'twitter',
                'label'       => __( 'Twitter', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Twitter', 'odin' ),
                'priority'		=> 10,
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'youtube',
                'label'       => __( 'Youtube', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Youtube', 'odin' ),
                'priority'		=> 10,
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'text',
                'settings'		=> 'mercado-livre',
                'label'       	=> __( 'Nick do Mercado Livre', 'odin' ),
                'section'		=> 'social',
                'description'	=> esc_attr__( 'Adicione o link completo para o seu Mercado Livre', 'odin' ),
                'priority'		=> 10,
            ) );

            /**
             * Custom Social
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', array(
                'type'        => 'slider',
                'settings'    => 'social_icons_radius',
                'label'       => esc_attr__( 'Bordas arredondadas dos ícones das redes sociais', 'odin' ),
                'section'     => 'social',
                'default'     => 50,
                'choices'     => array(
                    'min'  => '0',
                    'max'  => '100',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element'  => 'ul.list-social-icon li a.social-icon',
                        'property' => 'border-radius',
                        'units'    => '%',
                    )
                ),
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'social_icons_background',
                'label'			=> __( 'Cor de fundo dos ícones das redes sociais', 'odin' ),
                'section'		=> 'social',
                'default'		=> '#333333',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => 'ul.list-social-icon li a.social-icon',
                        'property' => 'background-color'
                    )
                ),
                'choices'     => array(
                    'alpha' => true,
                ),
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'social_icons_background_hover',
                'label'			=> __( 'Cor de fundo dos ícones das redes sociais quando o0 cursor está em cima do ícone', 'odin' ),
                'section'		=> 'social',
                'default'		=> '#222222',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => 'ul.list-social-icon li a.social-icon:hover',
                        'property' => 'background-color'
                    )
                ),
                'choices'     => array(
                    'alpha' => true,
                ),
            ) );

            Kirki::add_field( 'kirki_custom_config', array(
                'type'        => 'slider',
                'settings'    => 'social_icons_margin',
                'label'       => esc_attr__( 'Espaçamento entre os ícones das redes sociais', 'odin' ),
                'section'     => 'social',
                'default'     => 5,
                'choices'     => array(
                    'min'  => '0',
                    'max'  => '50',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element'  => 'ul.list-social-icon li.other-icon',
                        'property' => 'margin-left',
                        'units'    => 'px',
                    ),
                    array(
                        'element'  => 'ul.list-social-icon li.other-icon',
                        'property' => 'margin-right',
                        'units'    => 'px',
                    ),
                    array(
                        'element'  => 'ul.list-social-icon li.first-icon',
                        'property' => 'margin-right',
                        'units'    => 'px',
                    ),
                    array(
                        'element'  => 'ul.list-social-icon li.last-icon',
                        'property' => 'margin-left',
                        'units'    => 'px',
                    ),
                ),
            ) );


        }

        public function add_notices() {
            echo '<div class="notice notice-warning is-dismissible"><p>Para o seu site funcionar perfeitamente é preciso instalar e ativar o plugin <a href="' . admin_url('plugins.php?s=Kirki Customizer Framework&plugin_status=all') . '">Kirki Customizer Framework</a>.</p></div>';
        }

    }

    Excellence_Customizer_Social_Icons::get_instance();

}