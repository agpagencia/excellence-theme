<?php

/**
 * Include Kirki fields
 */
require( 'excellence-featured-page-home-customizer.php' );

if ( ! function_exists( 'excellence_get_featured_page_home' ) ) {

    /**
     * 
     * Print the selected page on Customizer
     * 
     * @since 18/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     */
    function excellence_get_featured_page_home() {

        $featured_page_home_id = get_theme_mod( 'featured_page_home_id', '' );

        if ( ! empty( $featured_page_home_id ) && is_numeric( $featured_page_home_id ) ) {

            $page = excellence_get_page_by_api( $featured_page_home_id );

            if ( $page ) {

                if ( ! empty( $featured_image = excellence_get_url_media_by_api( $page['featured_image'] ) ) ) {
                    echo '<section style="background-image:url(' . $featured_image . ')" class="wrap-featured-page-home">';
                } else {
                    echo '<section class="wrap-featured-page-home">';
                }

                    echo '<div class="container">';

                    if ( ! empty( $page['title'] ) ) {
                        
                        echo '<h2 class="title-line">';
                        echo $page['title'];
                        echo '</h2><!-- /.title-line -->';

                    }

                    if ( ! empty( $page['content'] ) ) {

                        echo '<div class="entry-content">';
                        echo $page['content'];
                        echo '</div><!-- /. entry-content -->';

                    }

                    if ( ! empty( $page['link'] ) ) {

                        echo '<a class="see-more" href="' . $page['link'] . '">Ver mais</a>';

                    }
                    
                    echo '</div><!-- /.container -->';

                    echo '<div class="overlay"></div>';

                echo '</section><!-- /.wrap-featured-page-home -->';
                
            }

        }

    }

}

/**
 * Default CSS to Featured Page Home
 */
add_action( 'wp_head', function() {

    if ( ! is_admin() ) {

        $css = "
            <style title'featured-page-home'>
                .wrap-featured-page-home {
                    margin: 0 -100%;
                    padding: 0 100%;
                }

                .wrap-featured-page-home .container {
                    text-align: center;
                }

                .wrap-featured-page-home .container a.see-more {
                    border-radius: 50px;
                    display: inline-block;
                    font-weight: 400;
                    margin-bottom: 20px;
                    padding: 10px 20px;
                }
            </style>
        ";

        echo $css;

    }

});