<?php

if ( ! class_exists( 'Excellence_Customizer_Featured_Page_Home' ) ) {

    class Excellence_Customizer_Featured_Page_Home {

        private static $instance;

        public static function get_instance() {

            if ( null == self::$instance ) {
                self::$instance = new self;
            }
            return self::$instance;

        }

        private function __construct() {

            /**
             * Verify if Kirki is activated.
             */
            if ( class_exists( 'Kirki' ) ) :
                $this->add_config();
                $this->add_panel();
                $this->add_sections();
                $this->add_fields();
            else:
                add_action( 'admin_notices', [$this, 'add_notices'] );
            endif;            

        }

        protected function add_config() {
            Kirki::add_config( 'kirki_custom_config', array(
                'capability'    => 'edit_theme_options',
                'option_type'   => 'theme_mod',
            ) );
        }

        protected function add_panel() {

            Kirki::add_panel( 'home', array(
                'priority'    => 10,
                'title'       => esc_html__( 'Home', 'kirki' ),
                'description' => esc_html__( 'Página Inicial', 'kirki' ),
            ) );

        }

        protected function add_sections() {

            /**
             * Featured Page on Home
             * ==============================================================================
             */
            Kirki::add_section( 'featured_page_home', array(
                'title'      => __( 'Página Destacada (na Home)' ),
                'priority'   => 10,
                'panel'      => 'home',
                'capability' => 'edit_theme_options',
            ) );
  
        }

        protected function add_fields() {

            /**
             * Featured Page on Home
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'dropdown-pages',
                'settings'    => 'featured_page_home_id',
                'label'       => esc_html__( 'Página', 'odin' ),
                'section'     => 'featured_page_home',
                'priority'    => 10,
            ] );

            /**
             * Custom Featured Page on Home
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'featured_page_home_font_color',
                'label'			=> __( 'Cor da fonte na página destacada na home', 'odin' ),
                'section'		=> 'featured_page_home',
                'default'		=> '#ffffff',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => '.wrap-featured-page-home',
                        'property' => 'color'
                    )
                )
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'featured_page_home_background',
                'label'			=> __( 'Cor de fundo da página destacada na home', 'odin' ),
                'section'		=> 'featured_page_home',
                'default'		=> '#666666',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => '.wrap-featured-page-home',
                        'property' => 'background-color'
                    )
                ),
                'choices'     => array(
                    'alpha' => true,
                ),
            ) );
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'select',
                'settings'    => 'featured_page_home_background_blend',
                'label'       => esc_html__( 'Efeito de mesclagem do background', 'odin' ),
                'section'     => 'featured_page_home',
                'default'     => 'normal',
                'placeholder' => esc_html__( 'Select an option...', 'odin' ),
                'priority'    => 10,
                'multiple'    => 1,
                'output' => array(
                    array(
                        'element'  => '.wrap-featured-page-home',
                        'property' => 'background-blend-mode'
                    )
                ),
                'choices'     => [
                    'normal'         => esc_html__( 'Normal', 'odin' ),
                    'multiply'       => esc_html__( 'Multiply', 'odin' ),
                    'screen'         => esc_html__( 'Screen', 'odin' ),
                    'overlay'        => esc_html__( 'Overlay', 'odin' ),
                    'darken'         => esc_html__( 'Darken', 'odin' ),
                    'lighten'        => esc_html__( 'Lighten', 'odin' ),
                    'color-dodge'    => esc_html__( 'Color Dodge', 'odin' ),
                    'saturation'     => esc_html__( 'Saturation', 'odin' ),
                    'color'          => esc_html__( 'Color', 'odin' ),
                    'luminosity	'    => esc_html__( 'Luminosity', 'odin' ),
                ],
            ] );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'featured_page_home_button',
                'label'			=> __( 'Cor do botão "Ver mais" da página destacada na home', 'odin' ),
                'section'		=> 'featured_page_home',
                'default'		=> '#000000',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => '.wrap-featured-page-home a.see-more',
                        'property' => 'background-color'
                    )
                ),
                'choices'     => array(
                    'alpha' => true,
                ),
            ) );
            Kirki::add_field( 'kirki_custom_config', array(
                'type'			=> 'color',
                'settings'		=> 'featured_page_home_button_font_color',
                'label'			=> __( 'Cor da fonte do botão "Ver mais" da página destacada na home', 'odin' ),
                'section'		=> 'featured_page_home',
                'default'		=> '#ffffff',
                'priority'		=> 10,
                'output' => array(
                    array(
                        'element'  => '.wrap-featured-page-home a.see-more',
                        'property' => 'color'
                    )
                )
            ) );
            
        }

        public function add_notices() {
            echo '<div class="notice notice-warning is-dismissible"><p>Para o seu site funcionar perfeitamente é preciso instalar e ativar o plugin <a href="' . admin_url('plugins.php?s=Kirki Customizer Framework&plugin_status=all') . '">Kirki Customizer Framework</a>.</p></div>';
        }

    }

    Excellence_Customizer_Featured_Page_Home::get_instance();

}