<?php

if ( ! class_exists( 'Excellence_Customizer_Media_Galley' ) ) {

    class Excellence_Customizer_Media_Galley {

        private static $instance;

        public static function get_instance() {

            if ( null == self::$instance ) {
                self::$instance = new self;
            }
            return self::$instance;

        }

        private function __construct() {

            /**
             * Verify if Kirki is activated.
             */
            if ( class_exists( 'Kirki' ) ) :
                $this->add_config();
                $this->add_panel();
                $this->add_sections();
                $this->add_fields();
            else:
                add_action( 'admin_notices', [$this, 'add_notices'] );
            endif;            

        }

        protected function add_config() {

            Kirki::add_config( 'kirki_custom_config', [
                'capability'    => 'edit_theme_options',
                'option_type'   => 'theme_mod',
            ] );

        }

        protected function add_panel() {

            Kirki::add_panel( 'modules', [
                'priority'    => 10,
                'title'       => esc_html__( 'Módulos', 'kirki' ),
                'description' => esc_html__( 'Configurações dos Módulos no seu Site', 'kirki' ),
            ] );

        }

        protected function add_sections() {

            /**
             * Featured Page on Home
             * ==============================================================================
             */
            Kirki::add_section( 'media_gallery', [
                'title'      => __( 'Galeria de Mídias' ),
                'priority'   => 10,
                'panel'      => 'modules',
                'capability' => 'edit_theme_options',
            ] );
  
        }

        protected function add_fields() {

            /**
             * Título para o módulo Galeria de Mídias
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'			=> 'text',
                'settings'		=> 'title_media_gallery',
                'label'			=> __( 'Título do módulo Galeria de Mídias', 'odin' ),
                'section'		=> 'media_gallery',
                'description'	=> esc_attr__( 'Adicione outro título para o módulo Galeria de Mídias no site.', 'odin' ),
                'default'		=> esc_attr__( 'Galerias', 'odin' ),
                'priority'		=> 10,
            ] );

            /**
             * Exibir o módulo Galeria de Mídias na Home?
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'     => 'switch',
                'settings' => 'show_home_media_gallery',
                'label'    => __( 'Exibir o módulo Galeria de Mídias na Home?', 'odin' ),
                'section'  => 'media_gallery',
                'default'  => 'off',
                'priority' => 10,
                'choices'  => [
                    'on'  => esc_attr__( 'Sim', 'odin' ),
                    'off' => esc_attr__( 'Não', 'odin' ),
                ],
            ] );

            /**
             * Quantidade de itens para a Galeria de Mídias na Home
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'select',
                'settings'    => 'qty_home_media_gallery',
                'label'       => __( 'Quantidade de Galerias para exibir na Home', 'odin' ),
                'section'     => 'media_gallery',
                'default'     => '4',
                'priority'    => 10,
                'multiple'    => 0,
                'choices'     => array(
                    '2'		=> esc_attr__( '2 galerias', 'odin' ),
                    '3'		=> esc_attr__( '3 galerias', 'odin' ),
                    '4'		=> esc_attr__( '4 galerias', 'odin' ),
                    '6'		=> esc_attr__( '6 galerias', 'odin' ),
                    '12'	=> esc_attr__( '12 galerias', 'odin' ),
                )
            ] );

            /**
             * Layout de exibição dos itens da Galeria de Mídias na Home
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'select',
                'settings'    => 'layout_home_media_gallery',
                'label'       => __( 'Layout de exibição dos itens da Galeria de Mídias na Home', 'odin' ),
                'section'     => 'media_gallery',
                'default'     => '1',
                'priority'    => 10,
                'multiple'    => 0,
                'choices'     => array(
                    '1'		=> esc_attr__( 'Últimos itens adicionados (fotos e/ou vídeos)', 'odin' ),
                    '2'		=> esc_attr__( 'Exibir um vídeo e os demais itens fotos', 'odin' ),
                    '3'		=> esc_attr__( 'Exibir apenas fotos', 'odin' ),
                    '4'		=> esc_attr__( 'Exibir apenas vídeos', 'odin' ),
                )
            ] );
            
        }

        public function add_notices() {
            echo '<div class="notice notice-warning is-dismissible"><p>Para o seu site funcionar perfeitamente é preciso instalar e ativar o plugin <a href="' . admin_url('plugins.php?s=Kirki Customizer Framework&plugin_status=all') . '">Kirki Customizer Framework</a>.</p></div>';
        }

    }

    Excellence_Customizer_Media_Galley::get_instance();

}