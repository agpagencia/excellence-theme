<?php
/**
 * 
 * Archive do módulo
 * 
 * @since 28/02/2020
 * @author Everaldo Matias <https://everaldo.dev>
 * 
 * @version 1.0 - 28/02/2020
 * 
 */
get_header();

/* Retorna o estilo definido para os títulos no Customizer */
$title_style = get_theme_mod( 'title_style' );

?>

	<section id="primary" class="col-1">
		<main id="main-content" class="site-main container" role="main">

			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<?php
					$title_media_gallery = get_theme_mod( 'title_media_gallery', esc_attr__( 'Galerias', 'excellence' ) );						
					echo '<h1 class="page-title title-style ' . $title_style . '">' . apply_filters( 'the_title', $title_media_gallery ) . '</h1>';
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header><!-- .page-header -->

				<?php

					// Start the Loop.
					while ( have_posts() ) : the_post();

						get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery' );

					endwhile;

						// Page navigation.
						odin_paging_nav();

				else :

					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

			endif; ?>

		</main><!--/ #main -->
	</section><!-- /#primary -->

<?php
get_footer();