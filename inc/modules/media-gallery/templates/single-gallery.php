<?php
/**
 * 
 * Single do módulo
 * 
 * @since 28/02/2020
 * @author Everaldo Matias <https://everaldo.dev>
 * 
 * @version 1.0 - 28/02/2020
 * 
 */
get_header();

/* Retorna o estilo definido para os títulos no Customizer */
$title_style = get_theme_mod( 'title_style' );

/**
 * Retorna o ID do post atual
 */
$id = get_the_ID();

/**
 * Retorna informações da galeria
 */
$media_gallery_type        = excellence_get_media_gallery_type();
$media_gallery_description = get_post_meta( $id, 'media_gallery_description', true );
$media_gallery_images      = get_post_meta( $id, 'media_gallery_images', true );
$media_gallery_videos      = get_post_meta( $id, 'media_gallery_videos', true );

?>

	<section id="primary" class="col-1">
		<main id="main-content" class="site-main container" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<header class="page-header">
					<h1 class="page-title title-style <?php echo $title_style; ?>"><?php the_title(); ?></h1>
				</header><!-- .page-header -->

				<div class="entry-gallery-images">

					<?php if ( $media_gallery_images ) : ?>

						<h3>Fotos</h3>
					
						<?php foreach( $media_gallery_images as $image_id ) {

							$image = wp_get_attachment_image_url( $image_id );
							
							if ( ! empty( $image ) ) {
								echo '<div class="wrap-media-photos">';
									echo '<a data-fancybox="gallery" href="' . esc_url( $image ) . '">';
										echo '<img src="' . esc_url( $image ) . '">';
									echo '</a>';
								echo '</div><!-- /.wrap-media -->';
							}

						} ?>

					<?php endif; ?>

				</div><!-- /.entry-gallery-images -->

				<div class="entry-gallery-videos">

					<?php if ( have_rows( 'media_gallery_videos' ) ) : ?>

						<h3>Vídeos</h3>

						<?php while ( have_rows( 'media_gallery_videos' ) ) : the_row(); 

							$video = get_sub_field( 'media_gallery_videos_youtube' );
							
							if ( $video ) {

								echo '<div class="wrap-media-videos">';
									echo '<a data-fancybox data-width="750" data-height="450" href="' . esc_url( $video ) . '">';
										echo '<img src="' . excellence_get_youtube_thumbnail_url( $video ) . '">';
									echo '</a>';
								echo '</div><!-- /.wrap-media -->';

							} ?>

						<?php endwhile; ?>

					<?php endif; ?>

				</div><!-- /.entry-gallery-videos -->
						
				<div class="entry-content">
					<?php echo apply_filters( 'the_content', $media_gallery_description ); ?>
				</div><!-- /.entry-content -->

			<?php endwhile; ?>

		</main><!--/#main -->
	</section><!-- /#primary -->
				
<?php
get_footer();
