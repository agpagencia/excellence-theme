<?php
echo '<div class="' . excellence_get_media_gallery_type() . ' each-media-gallery first-media-gallery-video">';

if ( have_rows( 'media_gallery_videos' ) ) :

    while ( have_rows( 'media_gallery_videos' ) ) : the_row(); 

        $video = get_sub_field( 'media_gallery_videos_youtube' );
        
        if ( $video ) {
           
            echo '<a data-fancybox data-width="750" data-height="450" href="' . esc_url( $video ) . '">';
                echo '<div class="thumb">';
                    echo '<img src="' . excellence_get_youtube_thumbnail_url( $video ) . '">';
                    echo '<div class="icon"></div><!-- /.icon -->';
                echo '</div><!-- /.thumb -->';
            echo '</a>';
            
            echo '<a href="' . esc_url( get_the_permalink() ) . '">';
                echo '<div class="title">';
                    the_title();
                echo '</div><!-- /.title -->';
            echo '</a>';

        }

        break;

    endwhile;

endif;

echo '</div><!-- /.each-media-gallery.first-media-gallery-video -->';