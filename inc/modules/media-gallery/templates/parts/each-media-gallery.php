<?php

// Classes
$excellence_get_class_by_qtd = excellence_get_class_by_qtd( get_theme_mod( 'qty_home_media_gallery', 4 ) );

echo '<div class="' . $excellence_get_class_by_qtd . ' ' . excellence_get_media_gallery_type() . ' each-media-gallery">';
    echo '<a href="' . esc_url( get_the_permalink() ) . '">';
        echo '<div class="thumb">';

            if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'medium' );
            } else {
                echo '<img class="featured-image" src="http://via.placeholder.com/768" alt="' . apply_filters( 'the_title', get_the_title() ) . '" />';
            }

            echo '<div class="icon"></div><!-- /.icon -->';

        echo '</div><!-- /.thumb -->';
        echo '<div class="title">';
            the_title();
        echo '</div><!-- /.title -->';
    echo '</a>';
echo '</div><!-- /.each-media-gallery -->';