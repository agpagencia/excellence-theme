<?php

// Prevents direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include Kirki fields
 */
require( 'excellence-media-gallery-customizer.php' );

if ( ! function_exists( 'excellence_media_gallery_cpt' ) ) {

    // Embed the Excellence_Term_Meta class if it does not exist
    if ( ! class_exists( 'Excellence_Post_Type' ) ) {
        require get_template_directory() . '/inc/classes/excellence-post-type.php'; 
    }

    // Embed the Excellence_Term_Meta class if it does not exist
    if ( ! class_exists( 'Excellence_Term_Meta' ) ) {
        require get_template_directory() . '/inc/classes/excellence-term-meta.php';
    }

    // Adiciona os campos personalizados para a galeria de mídias caso o plugin ACF esteja ativo
    if ( function_exists( 'get_field' ) ) {
        require get_template_directory() . '/inc/modules/media-gallery/excellence-media-gallery-fields.php';
    }

    /**
     * @version 1.0 - 26/02/2020
     */
    function excellence_media_gallery_cpt() {

        /**
         * 
         */

        $singular_name = 'Galeria de Mídia';
        $plural_name   = 'Galeria de Mídias';
        $slug_name     = 'gallery';

        /**
         * Instância a classe Excellence_Post_Type para criar um novo CPT
         */
        $cpt = new Excellence_Post_Type( $singular_name, $slug_name );

        /**
         * Define os labels do CPT.
         * @link https://developer.wordpress.org/reference/functions/register_post_type/
         */
        $cpt->set_labels(
            array(
                'all_items'    => __( 'Todas Galerias', 'excellence' ),
                'add_new'      => __( 'Adicionar nova', 'excellence' ),
                'add_new_item' => __( 'Adicionar nova ' . $singular_name, 'excellence' ),
                'menu_name'    => __( $plural_name, 'excellence' ),
                'not_found'    => __( 'Nenhuma Galeria encontrada!', 'excellence' )
            )
        );

        /**
         * Define os argumentos do CPT.
         * @link https://developer.wordpress.org/reference/functions/register_post_type/#parameters
         */
        $cpt->set_arguments(
            array(
                'supports' => array( 'title' ),
                'menu_icon' => 'dashicons-format-gallery'
            )
        );

    }
    add_action( 'init', 'excellence_media_gallery_cpt', 1 );

}

if ( ! function_exists( 'excellence_get_media_gallery_type' ) ) {

    /**
     * 
     * Retorna o tipo de mídia utilizado, sendo:
     * 1 - Fotos
     * 2 - Vídeos
     * 3 - Ambos, Fotos e Vídeos
     * 
     * @since 26/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 26/02/2020
     * 
     * @param int $qty of items to apply class (required)
     * @param string $return to the class
     * 
     * @return string with class
     * 
     */
    function excellence_get_media_gallery_type( $return = 'slug' ) {

        $media_gallery_type = get_post_meta( get_the_ID(), 'media_gallery_type', true );

        if ( $media_gallery_type == 1 ) {

            if ( $return == 'slug' ) {

                return 'media-photos';

            } elseif ( $return == 'title' ) {

                return 'Fotos';

            } else {

                die( 'O parâmetro da função deve ser slug ou title' );

            }

        } elseif ( $media_gallery_type == 2 ) {

            if ( $return == 'slug' ) {

                return 'media-videos';

            } elseif ( $return == 'title' ) {

                return 'Vídeos';

            } else {

                die( 'O parâmetro da função deve ser slug ou title' );

            }

        } elseif ( $media_gallery_type == 3 ) {

            if ( $return == 'slug' ) {

                return 'media-photos-videos';

            } elseif ( $return == 'title' ) {

                return 'Fotos e Vídeos';

            } else {

                die( 'O parâmetro da função deve ser slug ou title' );

            }

        } else {

            return __( 'Tipo de mídia não identificado', 'odin' );

        }

    }

}

if ( ! function_exists( 'excellence_get_media_gallery_home' ) ) {

    /**
     * 
     * Imprime o loop de galerias na home
     * 
     * @since 27/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 2.0 - 02/03/2020
     * 
     */
    function excellence_get_media_gallery_home() {

        $show_home_media_gallery = get_theme_mod( 'show_home_media_gallery' );

        if ( $show_home_media_gallery ) {

            /**
             * Define a quantidade de itens para exibir na Home
             */
            $args[ "posts_per_page" ] = get_theme_mod( 'qty_home_media_gallery', 4 );

            // Define o CPT no Query		
            $args[ "post_type" ] = 'gallery';

            // Define o status dos itens no Query		
            $args[ "post_status" ] = 'publish';
        
            /**
             * Retorna o estilo definido para os títulos no Customizer
             */
            $title_style = get_theme_mod( 'title_style' );

            // Contador
            $count = 0;

            echo '<div class="wrapper-media-gallery">';

                echo '<div class="container">';

                    echo '<h2 class="title-style ' . $title_style . '">' . get_theme_mod( 'title_media_gallery', 'Galerias' ) . '</h2>';

                    // Verifica o layout escolhido para exibir a galeria de mídias na Home
                    $layout_home_media_gallery = get_theme_mod( 'layout_home_media_gallery', 1 );

                    if ( 1 == $layout_home_media_gallery ) {

                        // Últimos itens adicionados (fotos e vídeos)

                        // Query
                        $query = new WP_Query( $args );

                        // Loop
                        while ( $query->have_posts() ) {
                            $query->the_post();
                            
                            // Incrementa a quantidade de voltas no Loop
                            $count++;

                            get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery' );

                            // Adiciona o hook each_media_gallery_{$count} após cada volta do loop
                            $hook = "each_media_gallery_" . $count;
                            do_action( $hook );

                        } // Endwhile

                    } elseif ( 2 == $layout_home_media_gallery ) {

                        // Exibir um vídeo e os demais itens fotos

                        // Define a quantidade de itens para exibir na Home removendo o primeiro, que será video
                        $posts_per_page_minus_one = $args[ "posts_per_page" ] - 1;

                        // Retorna apenas um item de vídeo
                        $args[ "posts_per_page" ] = 1;
                        
                        // Retorna apenas um item que possua vídeo
                        $args[ "meta_query" ] = [
                            'relation'		=> 'OR',
                            array(
                                'key'		=> 'media_gallery_type',
                                'value'		=> '2',
                                'compare'	=> 'OR'
                            ),
                            array(
                                'key'		=> 'media_gallery_type',
                                'value'		=> '3',
                                'compare'	=> 'OR'
                            )
                        ];

                        // Query
                        $query = new WP_Query( $args );

                        // Loop
                        while ( $query->have_posts() ) {
                            $query->the_post();

                            $post__not_in = get_the_ID();

                            // Incrementa a quantidade de voltas no Loop
                            $count++;

                            get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery-video' );

                            // Adiciona o hook each_media_gallery_{$count} após cada volta do loop
                            $hook = "each_media_gallery_" . $count;
                            do_action( $hook );

                        } // Endwhile

                        wp_reset_postdata(); // Reset Query

                        // Define a quantidade de itens para exibir na Home removendo o primeiro, que será video
                        $args[ "posts_per_page" ] = $posts_per_page_minus_one;

                        // Retorna os demais itens que sejam apenas fotos
                        $args[ "meta_query" ] = [
                            array(
                                'key'		=> 'media_gallery_type',
                                'value'		=> '1'
                            )
                        ];

                        $args[ "post__not_in" ] = [ $post__not_in ];

                        // Query
                        $query = new WP_Query( $args );
                        
                        // Loop
                        while ( $query->have_posts() ) {
                            $query->the_post();

                            // Incrementa a quantidade de voltas no Loop
                            $count++;
                            
                            get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery' );

                            // Adiciona o hook each_media_gallery_{$count} após cada volta do loop
                            $hook = "each_media_gallery_" . $count;
                            do_action( $hook );

                        } // Endwhile


                    } elseif ( 3 == $layout_home_media_gallery ) {

                        // Retorna os demais itens que sejam apenas fotos
                        $args[ "meta_query" ] = [
                            array(
                                'key'		=> 'media_gallery_type',
                                'value'		=> '1'
                            )
                        ];

                        // Query
                        $query = new WP_Query( $args );
                        
                        // Loop
                        while ( $query->have_posts() ) {
                            $query->the_post();

                            // Incrementa a quantidade de voltas no Loop
                            $count++;
                            
                            get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery' );

                            // Adiciona o hook each_media_gallery_{$count} após cada volta do loop
                            $hook = "each_media_gallery_" . $count;
                            do_action( $hook );

                        } // Endwhile

                    } elseif ( 4 == $layout_home_media_gallery ) {

                        // Retorna os demais itens que sejam apenas fotos
                        $args[ "meta_query" ] = [
                            array(
                                'key'		=> 'media_gallery_type',
                                'value'		=> '2'
                            )
                        ];

                        // Query
                        $query = new WP_Query( $args );
                        
                        // Loop
                        while ( $query->have_posts() ) {
                            $query->the_post();

                            // Incrementa a quantidade de voltas no Loop
                            $count++;
                            
                            get_template_part( '/inc/modules/media-gallery/templates/parts/each-media-gallery' );

                            // Adiciona o hook each_media_gallery_{$count} após cada volta do loop
                            $hook = "each_media_gallery_" . $count;
                            do_action( $hook );

                        } // Endwhile

                    } else {

                        die( 'Nada por aqui!' );

                    }

                    echo '<div class="link-to-all">';
                        echo '<a href="' . esc_url( home_url( 'gallery' ) ) . '">' . __( 'Ver todas Galerias', 'odin' ) . '</a>';
                    echo '</div><!-- /.link-to-all -->';

                echo '</div><!-- /.container -->';

            echo '</div><!-- /.wrapper-media-gallery -->';
            
            wp_reset_postdata(); // Reset Query

        } // Endif

    }

}

if ( ! function_exists( 'excellence_media_gallery_stylesheet' ) ) {

    /**
     * 
     * Imprime CSS do módulo
     * 
     * @since 27/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 27/02/2020
     * 
     */
    function excellence_media_gallery_stylesheet() {

        if ( ! is_admin() ) {

            $css = '
                <style type="text/css" id="media-gallery-stylesheet">
                        
                    .wrapper-media-gallery,
                    .wrapper-media-gallery .container {
                        display: flex;
                        flex-wrap: wrap;
                        padding-left: 10px;
                        padding-right: 10px;
                        width: 100%;
                    }

                    .wrapper-media-gallery .title-style,
                    .wrapper-media-gallery .link-to-all {
                        flex-basis: 100%;
                        text-align: center;
                        margin: 20px;
                    }

                    .wrapper-media-gallery .title-style {
                        text-transform: uppercase;
                    }

                    .wrapper-media-gallery [class*="col-"] {
                        padding: 10px;
                    }

                    .wrapper-media-gallery .col-2 {
                        flex-basis: 50%;
                        max-width: 50%;
                    }

                    .wrapper-media-gallery .col-3 {
                        flex-basis: calc(100% / 3);
                        max-width: calc(100% / 3);
                    }

                    .wrapper-media-gallery .col-4,
                    .wrapper-media-gallery .col-8 {
                        flex-basis: 25%;
                        max-width: 25%;
                    }

                    .wrapper-media-gallery .col-6,
                    .wrapper-media-gallery .col-12 {
                        flex-basis: calc(100% / 6);
                        max-width: calc(100% / 6);
                    }

                    .wrapper-media-gallery .each-media-gallery a .thumb {
                        margin-bottom: 10px;
                    }

                    .wrapper-media-gallery .each-media-gallery a .title {
                        text-align: center;
                    }

                    .wrapper-media-gallery .link-to-all a {
                        text-transform: uppercase;
                        padding: 10px 20px;
                        background-color: #ccc;
                        border-radius: 5px;
                    }

                    @media (max-width: 800px) {

                        .wrapper-media-gallery .col-6 {
                                flex-basis: calc(100% / 3);
                            max-width: calc(100% / 3);
                            }
                            
                        .wrapper-media-gallery .col-12 {
                            flex-basis: 25%;
                            max-width: 25%;
                        }	

                    }

                    @media (max-width: 600px) {
                        .wrapper-media-gallery [class*="col-"] {
                            flex-basis: 50%;
                            max-width: 50%;
                        }
                    }

                    @media (max-width: 400px) {
                        .wrapper-media-gallery [class*="col-"] {
                            flex-basis: 100%;
                            max-width: 100%;
                        }
                    }

                </style>
            ';

            echo $css;

        }

    }

    add_action( 'wp_head', 'excellence_media_gallery_stylesheet', 99 );

}

/**
 * 
 * Adiciona os templates específicos para o módulo
 * 
 * @since 28/02/2020
 * @author Everaldo Matias <https://everaldo.dev>
 * 
 * @version 1.0 - 28/02/2020
 * 
 */

if ( ! function_exists( 'excellence_media_gallery_single_template' ) ) {
    
    function excellence_media_gallery_single_template( $single_template ) {
        
        global $post;

        if ( 'gallery' == $post->post_type ) {
            $single_template = get_stylesheet_directory() . '/inc/modules/media-gallery/templates/single-gallery.php';
        }
        return $single_template;
        
    }

    add_filter( 'single_template', 'excellence_media_gallery_single_template' );

}

if ( ! function_exists( 'excellence_media_gallery_archive_template' ) ) {

    function excellence_media_gallery_archive_template( $archive_template ) {
    
        global $post;

        if ( is_archive() && 'gallery' == get_post_type() ) {
            $archive_template = get_stylesheet_directory() . '/inc/modules/media-gallery/templates/archive-gallery.php';
        }
        return $archive_template;

    }

    add_filter( 'archive_template', 'excellence_media_gallery_archive_template' );

}
