<?php

// Prevents direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include Kirki fields
 */
require( 'excellence-popup-customizer.php' );

/**
 * Verifica se o módulo Popup deve ser usado
 */
$use_popup = get_theme_mod( 'use_popup' );
if ( $use_popup ) {

    add_action( 'wp_footer', 'excellence_popup' );

}

if ( ! function_exists( 'excellence_popup' ) ) {

    function excellence_popup() {

        $image_popup = get_theme_mod( 'image_popup' );

        if ( $image_popup && is_home() || is_front_page() ) :

            $link_popup = get_theme_mod( 'link_popup' );
        ?>

            <div id="alertModal" class="modal">

                <div class="modal-content">
                    <span id="closeModal" class="close">&times;</span>

                    <?php if( $link_popup ) : ?>

                        <a href="<?php echo esc_url( $link_popup ); ?>">
                            <img src="<?php echo esc_url( $image_popup ); ?>" alt="<?php bloginfo( 'name' ); ?>">
                        </a>

                    <?php else : ?>

                        <img src="<?php echo esc_url( $image_popup ); ?>" alt="<?php bloginfo( 'name' ); ?>">

                    <?php endif; ?>

                </div><!-- /.modal-content -->

            </div><!-- /.modal -->

        <?php
        endif;
    }

}

if ( ! function_exists( 'excellence_popup_stylesheet' ) ) {

    /**
     * 
     * Imprime CSS do módulo
     * 
     * @since 23/03/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 23/03/2020
     * 
     */
    function excellence_popup_stylesheet() {

        if ( ! is_admin() ) {

            $css = '
                <style type="text/css" id="popup-stylesheet">
                        
                    /* The Modal (background) */
                    #alertModal {
                        display: none; /* Hidden by default */
                        position: fixed; /* Stay in place */
                        z-index: 1; /* Sit on top */
                        left: 0;
                        top: 0;
                        width: 100%; /* Full width */
                        height: 100%; /* Full height */
                        overflow: auto; /* Enable scroll if needed */
                        background-color: rgb(0,0,0); /* Fallback color */
                        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                    }

                    /* Modal Content/Box */
                    #alertModal .modal-content {
                        background-color: #fefefe;
                        border-radius: 7px;
                        margin: 15% auto; /* 15% from the top and centered */
                        padding: 20px;
                        position: relative;
                        max-width: 700px; /* Could be more or less, depending on screen size */
                    }
                    #alertModal .modal-content img {
                        border-radius: 7px;
                        height: auto;
                        max-width: 100%;
                    }

                    /* The Close Button */
                    #alertModal .modal-content .close {
                        background-color: red;
                        border-radius: 50px;;
                        color: #fff;
                        font-size: 22px;
                        font-weight: bold;
                        height: 25px;
                        position: absolute;
                        width: 25px;
                        padding: 0.5px 0 0 7px;
                        right: -10px;
                        top: -10px;
                    }

                    #alertModal .modal-content .close:hover,
                    #alertModal .modal-content .close:focus {
                        background-color: #fff;
                        color: red;
                        text-decoration: none;
                        cursor: pointer;
                    }

                </style>
            ';

            echo $css;

        }

    }

    add_action( 'wp_head', 'excellence_popup_stylesheet', 99 );

}

if ( ! function_exists( 'excellence_popup_javascript' ) ) {

    function excellence_popup_javascript() {

        if ( ! is_admin() ) {

            $js = '
                <script type="text/javascript">
                    document.addEventListener( "DOMContentLoaded", ()=>{

                        if ( document.getElementById("alertModal") ) {

                            var modal = document.getElementById("alertModal");
                            modal.style.display = "block";

                            var close = document.getElementById("closeModal");

                            close.onclick = function () {
                                modal.style.display = "none";
                            },

                            window.onclick = function (event) {
                                if ( event.target == modal ) {
                                    modal.style.display = "none";
                                }
                            }
                            
                        }

                    });
                </script>
            ';

            echo $js;

        }

    }

    add_action( 'wp_footer', 'excellence_popup_javascript', 99 );

}