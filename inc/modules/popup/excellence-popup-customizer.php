<?php

if ( ! class_exists( 'Excellence_Customizer_Popup' ) ) {

    class Excellence_Customizer_Popup {

        private static $instance;

        public static function get_instance() {

            if ( null == self::$instance ) {
                self::$instance = new self;
            }
            return self::$instance;

        }

        private function __construct() {

            /**
             * Verify if Kirki is activated.
             */
            if ( class_exists( 'Kirki' ) ) :
                $this->add_config();
                $this->add_panel();
                $this->add_sections();
                $this->add_fields();
            else:
                add_action( 'admin_notices', [$this, 'add_notices'] );
            endif;            

        }

        protected function add_config() {

            Kirki::add_config( 'kirki_custom_config', [
                'capability'    => 'edit_theme_options',
                'option_type'   => 'theme_mod',
            ] );

        }

        protected function add_panel() {

            Kirki::add_panel( 'modules', [
                'priority'    => 10,
                'title'       => esc_html__( 'Módulos', 'kirki' ),
                'description' => esc_html__( 'Configurações dos Módulos no seu Site', 'kirki' ),
            ] );

        }

        protected function add_sections() {

            /**
             * Popup
             * ==============================================================================
             */
            Kirki::add_section( 'popup', [
                'title'      => __( 'Popup' ),
                'priority'   => 10,
                'panel'      => 'modules',
                'capability' => 'edit_theme_options',
            ] );
  
        }

        protected function add_fields() {

            /**
             * Usar o módulo de Popup?
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'     => 'switch',
                'settings' => 'use_popup',
                'label'    => __( 'Usar o módulo de Popup', 'odin' ),
                'section'  => 'popup',
                'default'  => 'off',
                'priority' => 10,
                'choices'  => [
                    'on'  => esc_attr__( 'Sim', 'odin' ),
                    'off' => esc_attr__( 'Não', 'odin' ),
                ],
            ] );

            /**
             * Imagem para o Popup
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'image',
                'settings'    => 'image_popup',
                'label'       => esc_html__( 'Imagem para o Popup', 'kirki' ),
                'section'     => 'popup',
                'priority'		=> 10,
            ] );

            /**
             * Link para a imagem para o Popup
             * ==============================================================================
             */
            Kirki::add_field( 'kirki_custom_config', [
                'type'        => 'link',
                'settings'    => 'link_popup',
                'label'       => __( 'Link para a imagem do Popup', 'kirki' ),
                'description' => esc_html__( 'Opcional', 'kirki' ),
                'section'     => 'popup',
                'priority'    => 10,
            ] );
            
        }

        public function add_notices() {
            echo '<div class="notice notice-warning is-dismissible"><p>Para o seu site funcionar perfeitamente é preciso instalar e ativar o plugin <a href="' . admin_url('plugins.php?s=Kirki Customizer Framework&plugin_status=all') . '">Kirki Customizer Framework</a>.</p></div>';
        }

    }

    Excellence_Customizer_Popup::get_instance();

}