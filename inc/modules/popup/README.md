# Módulo Popup #

Módulo para adicionar um Popup na Home do site, normalmente utilizado para algum recado/informe importante, com imagem e link (opcional).

### Arquivos/Pasta Necessários ###

Sugestão de pasta '../inc/modules/popup/'

excellence-popup.php

excellence-popup-customizer.php

### Uso ###

#### Carregue o arquivo principal ####

No functions.php do tema, adicione:

```
require( 'inc/modules/popup/excellence-popup.php' );
```

Pronto, o módulo Popup já deve funcionar. Para usá-lo acesse o painel administrativo do site e acesse Aparência/Personalizar/Módulos/Popup.

### Changelog ###

v1.0 - 23/03/2020 - Verão inicial