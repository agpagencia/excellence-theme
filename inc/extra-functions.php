<?php

if ( ! function_exists( 'excellence_get_class_by_qtd' ) ) {

    /**
     * 
     * Retorna string para ser usada como classe CSS de acordo com a quantidade de itens
     * 
     * @since 26/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 26/02/2020
     * 
     * @param int $qty of items to apply class (required)
     * @param string $prefix to the class
     * 
     * @return string with class
     * 
     */
    function excellence_get_class_by_qtd( $qty, $prefix = '' ) {

        if ( ! empty( $qty ) ) {
            $qty = $qty . ' ';
        }

        // Retorna a classe de acordo com a quantidade definida e o prefixo
        switch( $qty ) {
            case 2:
                return 'qty-' . $qty . 'col-2';
                break;
            case 3:
                return 'qty-' . $qty . 'col-3';
                break;
            case 4:
                return 'qty-' . $qty . 'col-4';
                break;
            case 6:
                return 'qty-' . $qty . 'col-6';
                break;
            case 8:
                return 'qty-' . $qty . 'col-4';
                break;
            case 12:
                return 'qty-' . $qty . 'col-12';
                break;
            default:
                return 'qty-' . $qty . 'col-3';
        }

    }

}

if ( ! function_exists( 'excellence_get_page_by_api' ) ) {

    /**
     * 
     * Get page by ID using Rest API
     * 
     * @since 18/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 18/02/2020
     * 
     * @param int $id of the page you want to return (required)
     * @param boolean $filter returned values (default = true)
     * 
     * @return array with four values ['link', 'title', 'content', 'featured_image']
     * 
     */
    function excellence_get_page_by_api( $id, $filter = true ) {

        $return = [];

        if ( ! empty( $id ) && is_numeric( $id ) ) {

            $request =  wp_remote_get( get_home_url() . '/wp-json/wp/v2/pages/' . $id );

        }

        if ( ! is_wp_error( $request ) ) {
            
            // Get the body of the response
            $response = wp_remote_retrieve_body( $request );
            
            // Decode the JSON in arrays
            $output = json_decode( $response, true );

            if ( $filter ) {

                $return['link']           = esc_url( $output['link'] );
                $return['title']          = apply_filters( 'the_title', $output['title']['rendered'] );
                $return['content']        = apply_filters( 'the_content', $output['content']['rendered'] );
                $return['featured_image'] = esc_attr( $output['featured_media'] );

            } else {

                $return['link']           = $output['link'];
                $return['title']          = $output['title']['rendered'];
                $return['content']        = $output['content']['rendered'];
                $return['featured_image'] = $output['featured_media'];

            }
            
            return $return;

        }         

    }

}

if ( ! function_exists( 'excellence_get_url_media_by_api' ) ) {

    /**
     * 
     * Get media url by ID using Rest API
     * 
     * @since 18/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 18/02/2020
     * 
     * @param int $id of the media you want to return (required)
     * 
     * @return string media url
     * 
     */
    function excellence_get_url_media_by_api( $id ) {

        $return = [];
        $request = '';

        if ( ! empty( $id ) && is_numeric( $id ) ) {

            $request = wp_remote_get( get_home_url() . '/wp-json/wp/v2/media/' . $id );

        }

        if ( ! is_wp_error( $request ) ) {
                
            // Get the body of the response
            $response = wp_remote_retrieve_body( $request );
            
            // Decode the JSON in arrays
            $output = json_decode( $response, true );

            return esc_url( $output['guid']['rendered'] );

        }

    }

}

if ( ! function_exists( 'excellence_get_youtube_thumbnail' ) ) {

    /**
     * 
     * Retorna a URL do thumbnail de um vídeo no YouTube
     * 
     * @since 28/02/2020
     * @author Everaldo Matias <https://everaldo.dev>
     * 
     * @version 1.0 - 28/02/2020
     * 
     * @return string url do vídeo no Youtube
     * 
     */   
    function excellence_get_youtube_thumbnail_url( $url ) {

        if ( strlen( $url ) > 11 ) {

            if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match ) ) {
                $id = $match[1];
            } else {
                $id = false;
            }

            $thumbnail_url = "https://img.youtube.com/vi/" . $id . "/hqdefault.jpg";

            return $thumbnail_url;

        }

    }

}