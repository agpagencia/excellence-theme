<?php

/**
 * Kirki Framework.
 * @link https://developer.wordpress.org/files/2014/10/Customizer-Object-Relationships.png
 */

Kirki::add_config( 'kirki_custom_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

/**
 * Includes Sections/Fields
 * ==============================================================================
 */
// include_once ( 'kirki/custom-kirki-dev.php' );
// include_once ( 'kirki/custom-kirki-header.php' );
// include_once ( 'kirki/custom-kirki-home.php' );
// include_once ( 'kirki/custom-kirki-footer.php' );

/**
 * Sessões
 * ==============================================================================
 */

/* Painel */
Kirki::add_panel( 'modules', array(
    'priority'    => 5,
    'title'       => esc_attr__( 'Módulos 3.0', 'odin' ),
    'description' => esc_attr__( 'Configurações gerais dos módulos.', 'odin' ),
    'capability'  => 'update_core'
) );

/* Seção Identidade do Site */
Kirki::add_section( 'identity', array(
    'title'          => __( 'Identidade do Site', 'odin' ),
    'panel'          => 'modules', // Not typically needed.
    'priority'       => 10,
    'capability'     => 'update_core',
    'theme_supports' => '', // Rarely needed.
) );
Kirki::add_field( 'kirki_custom_config', array(
    'type'        => 'custom',
    'settings'    => 'desc_identity',
    'label'       => '',
    'section'     => 'identity',
    'default'     => '<h2>Schema.org</h2><hr><span>Escolha uma categoria para aplicar ao site e melhorar o SEO de acordo com as regras do Schema.org. Segue link para a lista de possibilidades: <a href="https://schema.org/docs/schemas.html" target="_blank">Schema.org</a></span>',
    'priority'    => 0,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'schema_identity',
	'label'			=> __( 'Schema', 'blank' ),
	'section'		=> 'identity',
	'default'       => 'Organization',
	'priority'		=> 10,
) );

/* Títulos */
Kirki::add_section( 'titulos', array(
    'title'          => __( 'Títulos', 'odin' ),
    'panel'          => '', // Not typically needed.
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'radio-image',
	'settings'    => 'title_style',
	'label'       => esc_html__( 'Estilo de títulos', 'odin' ),
	'section'     => 'titulos',
	'default'     => 'with_bg_center',
	'priority'    => 10,
	'choices'     => array(
		'with_bg_center'	=> get_template_directory_uri() . '/assets/images/default/with_bg_center.png',
		'with_bg_left'		=> get_template_directory_uri() . '/assets/images/default/with_bg_left.png',
		'without_bg_center'	=> get_template_directory_uri() . '/assets/images/default/without_bg_center.png',
		'without_bg_left'	=> get_template_directory_uri() . '/assets/images/default/without_bg_left.png',
	),
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'		=> 'select',
	'settings'	=> 'title_uppercase',
	'label'		=> __( 'UPPERCASE x Original?', 'odin' ),
	'section'	=> 'titulos',
	'default'	=> 'uppercase',
	'priority'	=> 10,
	'multiple'	=> 0,
	'choices'	=> array(
		'uppercase'	=> esc_attr__( 'UPPERCASE', 'odin' ),
		'initial'	=> esc_attr__( 'Original', 'odin' ),
	),
	'output'	=> array(
		array(
			'element'  => array( '.title', '.title-line' ),
	        'property' => 'text-transform',
		),
	)
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'slider',
	'settings'    => 'border_radius',
	'label'       => esc_attr__( 'Bordas arredondadas dos títulos (com fundo)', 'odin' ),
	'section'     => 'titulos',
	'default'     => 5,
	'choices'     => array(
		'min'  => '0',
		'max'  => '20',
		'step' => '1',
	),
	'output' => array(
		array(
			'element'  => '.title.with_bg_center',
	        'property' => 'border-top-right-radius',
	        'units'    => 'px',
		),
		array(
			'element'  => '.title.with_bg_center',
	        'property' => 'border-top-left-radius',
	        'units'    => 'px',
		),
		array(
			'element'  => '.title.with_bg_left',
	        'property' => 'border-top-right-radius',
	        'units'    => 'px',
		),
		array(
			'element'  => '.title.with_bg_left',
	        'property' => 'border-top-left-radius',
	        'units'    => 'px',
		)
    ),
) );

/* Informações de Contato */

Kirki::add_section( 'contatos', array(
    'title'          => __( 'Informações de Contatos' ),
    'panel'          => '', // Not typically needed.
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'phone',
	'label'			=> __( 'Telefone', 'odin' ),
	'section'		=> 'contatos',
	'description'	=> esc_attr__( 'Adicione o número de contato principal', 'odin' ),
	'priority'		=> 10,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'mobile',
	'label'			=> __( 'Telefone para a versão mobile', 'odin' ),
	'section'		=> 'contatos',
	'description'	=> esc_attr__( 'Adicione o número de contato para ser exibido como contato direto na versão mobile', 'odin' ),
	'priority'		=> 10,
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'whatsapp',
	'label'			=> __( 'Whatsapp', 'odin' ),
	'section'		=> 'contatos',
	'description'	=> esc_attr__( 'Adicione o número de whatsapp para atendimento', 'odin' ),
	'priority'		=> 10,
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'email',
	'label'			=> __( 'E-mail', 'odin' ),
	'section'		=> 'contatos',
	'description'	=> esc_attr__( 'Adicione o e-mail principal de atendimento', 'odin' ),
	'priority'		=> 10,
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'text',
	'settings'		=> 'endereco',
	'label'       => __( 'Endereço', 'odin' ),
	'section'		=> 'contatos',
	'description'	=> esc_attr__( 'Adicione o endereço físico da empresa. Utilize um endereço que seja legível para o Google Maps, assim o mapa de localização será apresentado automaticamente', 'odin' ),
	'priority'		=> 10,
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'checkbox',
	'settings'    => 'show_maps',
	'label'    => __( 'Exibir o mapa na página Contato?', 'odin' ),
	'section'     => 'contatos',
	'default'     => '1',
	'priority'    => 10,
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'text',
	'settings'    => 'key_google_maps',
	'label'    => __( 'API Key do Google Maps', 'odin' ),
	'section'     => 'contatos',
	'description'	=> 'Caso não tenha uma API Key, poderá utilizar essa: AIzaSyCRo8ahHJbr1wU_umHtaW--wEE-AgB1ONU <br> Veja mais na documentação do <a href="https://developers.google.com/maps/documentation/embed/guide">Google</a>',
	'default'     => 'AIzaSyCRo8ahHJbr1wU_umHtaW--wEE-AgB1ONU',
	'priority'    => 10,
) );




/* ///////////////////////////////////////////// */


Kirki::add_section( 'colors', array(
    'title'          => __( 'Cores' ),
    'panel'          => '', // Not typically needed.
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );



Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'custom',
	'settings'    => 'title_links',
	'label'       => '',
	'section'     => 'colors',
	'default'     => '<h2>Links</h2><hr>',
	'priority'    => 10,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'color',
	'settings'		=> 'links_color',
	'label'			=> __( 'Cor dos link', 'odin' ),
	'section'		=> 'colors',
	'default'		=> '#333333',
	'priority'		=> 10,
	'description'	=> 'color{} para os links',
	'output' => array(
		array(
	        'element'  => 'a',
	        'property' => 'color',
	        //'units'    => ' !important'
	    ),
	    array(
	        'element'  => '.wrapper-banner .each a',
	        'property' => 'color',
	        //'units'    => ' !important'
	    )
    ),
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'color',
	'settings'		=> 'hover_links_color',
	'label'			=> __( 'Hover dos link', 'odin' ),
	'section'		=> 'colors',
	'default'		=> '#000000',
	'priority'		=> 10,
	'description'	=> 'color{} do hover dos links',
	'output' => array(
		array(
	        'element'  => 'a:hover',
	        'property' => 'color',
	        //'units'    => ' !important'
	    )
    ),
) );

Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'custom',
	'settings'    => 'title_titles',
	'label'       => '',
	'section'     => 'colors',
	'default'     => '<h2>Títulos</h2><hr>',
	'priority'    => 10,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'color',
	'settings'		=> 'bg_title_color',
	'label'    => __( 'Fundo dos títulos', 'odin' ),
	'section'		=> 'colors',
	'default'		=> '#000000',
	'priority'		=> 10,
	'description'	=> 'background-color{} para os itens com a classe .title-color e .title',
	'output' => array(
		array(
        	'element'  => '.title-color',
        	'property' => 'background-color',
        ),
        array(
        	'element'  => '.title',
        	'property' => 'background-color',
        )
    ),
    'choices'     => array(
		'alpha' => true,
	),
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'color',
	'settings'		=> 'font_title_color',
	'label'    => __( 'Fonte dos títulos', 'odin' ),
	'section'		=> 'colors',
	'default'		=> '#ffffff',
	'priority'		=> 10,
	'description'	=> 'color{} para os itens com a classe .title-color e .title',
	'output' => array(
		array(
        	'element'  => '.title-color',
        	'property' => 'color',
        ),
        array(
        	'element'  => '.title',
        	'property' => 'color',
        )
    ),
    'choices'     => array(
		'alpha' => true,
	),
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'custom',
	'settings'    => 'title_borders',
	'label'       => '',
	'section'     => 'colors',
	'default'     => '<h2>Bordas</h2><hr>',
	'priority'    => 10,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'color',
	'settings'		=> 'border_color',
	'label'    => __( 'Bordas', 'odin' ),
	'section'		=> 'colors',
	'default'		=> '#cccccc',
	'priority'		=> 10,
	'description'	=> 'border-color{} para os itens com a classe .title-color',
	'output' => array(
		array(
        	'element'  => '.title-color',
        	'property' => 'border-color'
        ),
        array(
        	'element'  => '.title',
        	'property' => 'border-color'
        )
    ),
) );

/**
 * Logo
 * ==============================================================================
 */
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'image',
	'settings'		=> 'logo',
	'label'    => __( 'Logotipo', 'odin' ),
	'section'		=> 'title_tagline',
	'priority'		=> 10,
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'image',
	'settings'		=> 'logo_footer',
	'label'    		=> __( 'Logotipo para o rodapé', 'odin' ),
	'description'	=> 'Use apenas quando precisar que a logomarca seja diferente a logomarca do Cabeçalho.',
	'section'		=> 'title_tagline',
	'priority'		=> 15,
) );

