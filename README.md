## Extra Functions ##

Arquivo para organizar e registrar funções úteis ao tema/site.

### Arquivos Necessários ###

Sugestão de pasta '../inc/'

extra-functions.php

### Uso ###

#### Carregue o arquivo ####

No functions.php do tema, adicione:

```
require( '../inc/extra-functions.php' );
```

## Class -> Excellence_Social_Icons ##

Classe para imprimir os ícones das redes sociais definidos no painel Personalizar do WP.

### Arquivos Necessários ###

Sugestão de pasta '../inc/classes/'

excellence-customizer-social-icons.php

excellence-social-icons.php

### Uso ###

#### Carregue a classe ####

```
require( '../path/to/excellence-social-icons.php' );
```

#### Adicionando os ícones das redes socias a um hook ####

Por padrão os ícones das redes sociais são adicionados ao hook 'excellence_header' na posição 100, caso precise removê-lo, use:

```
function excellence_remove_social_icons() {
    remove_action( 'excellence_header', 'the_social_icons', 100 );
}
add_action( 'init', 'excellence_remove_social_icons' );
```

Para adicionar os ícones das redes sociais em mais algum hook, use o seguinte, substituindo o nome do hook e a prioridade:

```
add_action( 'hook_name', 'the_social_icons', 10 );
```

## Module -> Featured Page Home ##

Módulo para adicionar ao painel Personalizar do WP a opção de escolher uma página de destaque para exibir na Home.

### Arquivos Necessários ###

Sugestão de pasta '../inc/modules/featured-page-home/'

excellence-featured-page-home.php

excellence-featured-page-home-customizer.php

### Uso ###

#### Carregue o arquivo principal ####

No functions.php do tema, adicione:

```
require( 'inc/modules/featured-page-home/excellence-featured-page-home.php' );
```

E adicione a função `<?php excellence_get_featured_page_home(); ?>` onde deseja exibir a página destacada na Home.

## Module -> Media Gallery ##

Módulo para adicionar uma galeria de fotos e vídeos no site.


### Arquivos/Pasta Necessários ###

/classes/excellence-post-type.php

/classes/excellence-term-meta.php

/media-gallery/excellence-media-gallery.php

/media-gallery/excellence-media-gallery-customizer.php

/media-gallery/excellence-media-gallery-fields.php


### Uso ###

#### Carregue o arquivo principal ####

No functions.php do tema, adicione:

```
require( 'inc/modules/media-gallery/excellence-media-gallery.php' );
```

E adicione a função `<?php excellence_get_media_gallery_home(); ?>` onde deseja exibir a galeria de mídias.


## Documentação dos Módulos ##

[Módulo Popup](https://gitlab.com/agpagencia/excellence-theme/-/blob/master/inc/modules/popup/README.md)
